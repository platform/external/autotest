# We don't care to run the performance or stress tests from dEQP-GLES on
# any boards -- they're very slow but don't generally fail.
dEQP-GLES.*.performance.*
dEQP-GLES.*.stress.*

# This set of tests may emit warnings, but is not required for
# conformance and I've never seen anyone pay attention to it.
dEQP-GLES.*.accuracy.*

# The deqp package ships an Android mustpass list instead of a normal
# Linux one, which helps us on host check for some extended behavior
# expectations from Android, but also has some expectations that host
# dEQP should *not* be trying to enforce (maximum Vulkan version,
# extensions exposed, layers exposed).  Skip until upstream dEQP can
# get fixed.  Related: https://gerrit.khronos.org/c/vk-gl-cts/+/5715
dEQP-VK.api.info.android.no_layers
dEQP-VK.api.info.android.no_unknown_extensions

