# Copyright 2022 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

from autotest_lib.client.common_lib import utils
from autotest_lib.server.site_tests.tast import tast

AUTHOR = 'Chromium OS Firmware EngProd team'
NAME = 'tast.firmware-bios-lvl1'
TIME = 'MEDIUM'
TEST_TYPE = 'Server'
DEPENDENCIES = 'servo_state:WORKING'
ATTRIBUTES = 'suite:faft_lv1'
MAX_RESULT_SIZE_KB = 1024 * 1024
PY_VERSION = 3
JOB_RETRIES = 0

# tast.py uses binaries installed from autotest_server_package.tar.bz2.
REQUIRE_SSP = True

DOC = '''
Run Tast tests for EC firmware.

The tests are part of 'group:firmware'. The 'firmware_level1' sub-attribute
limits it to level 1 AP bios tests.
'''

command_args, varslist = tast.split_arguments(args)
args_dict = utils.args_to_dict(command_args)
servo_args = hosts.CrosHost.get_servo_arguments(args_dict)

def run(machine):
    job.run_test('tast',
                 host=hosts.create_host(machine, servo_args=servo_args),
                 test_exprs=['("group:firmware" && firmware_level1)'],
                 ignore_test_failures=True, max_run_sec=10800,
                 command_args=command_args,
                 varslist=varslist)

parallel_simple(run, machines)
