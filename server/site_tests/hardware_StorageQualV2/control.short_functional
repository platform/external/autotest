# Copyright 2022 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

from autotest_lib.client.common_lib import utils

AUTHOR = 'dlunev, abergman, chromeos-engprod-platform-syd, chromeos-storage'
NAME = 'hardware_StorageQualV2.short_functional'
ATTRIBUTES = ''
TIME = 'LENGTHY'
TEST_CATEGORY = 'Stress'
TEST_CLASS = 'Hardware'
TEST_TYPE = 'Server'
ATTRIBUTES = 'suite:storage_qual_bringup'
PY_VERSION = 3
PRIORITY = 200

MAX_RESULT_SIZE_KB = 1024 * 1024
JOB_RETRIES = 0
REQUIRE_SSP = True

DOC = '''
Run the Tast-based storage qualification quick test.

Tast is an integration-testing framework analogous to the test-running portion
of Autotest. See https://chromium.googlesource.com/chromiumos/platform/tast/ for
more information.

See http://go/tast-failures for information about investigating failures.
'''
import tempfile
import yaml

def run(machine):
    with tempfile.NamedTemporaryFile(suffix='.yaml', mode='w+') as temp_file:
        # Writing test arguments to yaml file except for wrapper-related arguments.
        test_args = dict()
        test_args['tast_skip_setup_check'] = 'true'
        test_args['tast_suspend_block_timeout'] = '10m'
        yaml.dump(test_args, stream=temp_file, default_flow_style=False)

        job.run_test('tast',
                     host=hosts.create_host(machine),
                     test_exprs=['storage.FullQualificationStress.functional'],
                     ignore_test_failures=False,
                     max_run_sec=7200,
                     command_args=args,
                     varsfiles=[temp_file.name])

parallel_simple(run, machines)
