# Copyright 2022 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

AUTHOR = "mruthven"
NAME = "firmware_GSCSetAPROV1"
PURPOSE = "Verify GSC can set the AP RO hash"
ATTRIBUTES = "suite:faft_cr50_experimental"
TIME = "SHORT"
TEST_TYPE = "server"
DEPENDENCIES = "servo_state:WORKING"
PY_VERSION = 3

DOC = """
This test verifies GSC can set and clear the AP RO hash when the board id type
is erased.
"""

from autotest_lib.client.common_lib import error
from autotest_lib.server import utils

if 'args_dict' not in locals():
    args_dict = {}

args_dict.update(utils.args_to_dict(args))
servo_args = hosts.CrosHost.get_servo_arguments(args_dict)

def run(machine):
    host = hosts.create_host(machine, servo_args=servo_args)

    job.run_test("firmware_GSCSetAPROV1", host=host, cmdline_args=args,
                 full_args=args_dict)

parallel_simple(run, machines)
